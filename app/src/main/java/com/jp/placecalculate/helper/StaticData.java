package com.jp.placecalculate.helper;

import android.content.Context;

import java.util.ArrayList;

public class StaticData {
    Context context;

    public StaticData(Context context) {
        this.context = context;
    }

    public void classes(ArrayList<String> classes, ArrayList<String> classId){
        classes.add("Choose Class");
        classId.add("0");
        classes.add("ECE");
        classId.add("1");
        classes.add("EEE");
        classId.add("2");
        classes.add("CSC");
        classId.add("3");
        classes.add("IT");
        classId.add("4");
    }
}
