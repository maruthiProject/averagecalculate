package com.jp.placecalculate.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jp.placecalculate.activity.MainActivity;
import com.jp.placecalculate.db.DBHelper;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethod {
    Context context;
    private boolean isBackPressed=true;
    private DBHelper dbHelper;
    private DateListener dateListener;
    private String TAG=CommonMethod.class.getSimpleName();

    public CommonMethod(Context context) {
        this.context = context;
        dbHelper=new DBHelper(context);
    }

    public CommonMethod(Context context, DateListener dateListener) {
        this.context = context;
        this.dateListener = dateListener;
    }

    // set date on textView
    public String date() {
        SimpleDateFormat df = new SimpleDateFormat( "dd-MM-yyyy" );
        String visitor_date = df.format( Calendar.getInstance().getTime() );
        return df.format( Calendar.getInstance().getTime() );

    }

    public String time() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat( "hh:mm a" );
        String in_time = mdformat.format(calendar.getTime());
        return in_time;
    }

    public String timeHH() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat( "HH:mm" );
        String in_time = mdformat.format(calendar.getTime());
        return in_time;
    }

    public void handleBackPress(View view, final Context context, final Class _targetClass) {

        view.setFocusableInTouchMode( true );
        view.requestFocus();
        view.setOnKeyListener( new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (!isBackPressed) {
                        intentFinish( context, _targetClass );
                        isBackPressed = true;
                    } else if (isBackPressed) {
                        isBackPressed = false;
                    }
                    return true;
                }
                return false;

            }
        } );
    }
    public void intentFinish(Context _context, Class _targetClass) {
        Activity activity = (Activity) _context;
        Intent intent = new Intent( _context, _targetClass );
        activity.startActivity( intent );
        activity.finish();
    }
    public String randomNumber() {
        Random rand = new Random();
        String num = String.valueOf( rand.nextInt( 90000 ) + 10000 );
        return num;
    }

    public void imageView(ImageView image, Bitmap bitmap, Context context) {

        Glide.with( context ).asBitmap().load( bitmap ).apply( RequestOptions.circleCropTransform() ).into( image );
    }

    public void imageViewCircle(ImageView image, byte[] byteimg, Context context) {
        Bitmap bitmap= BitmapFactory.decodeByteArray(byteimg, 0, byteimg.length);
        Glide.with( context ).asBitmap().load( bitmap ).apply( RequestOptions.circleCropTransform() ).into( image );
    }
    public void imageView(ImageView image, byte[] byteimg, Context context) {
        Bitmap bitmap= BitmapFactory.decodeByteArray(byteimg, 0, byteimg.length);
        Glide.with( context ).asBitmap().load( bitmap ).into( image );
    }
    public byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }
    public boolean EmailValid(String emailAddress) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@" + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\." + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|" + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile( regExpn, Pattern.CASE_INSENSITIVE );
        Matcher matcher = pattern.matcher( emailAddress );
        return matcher.matches();
    }

    public void Logoutscreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder( context );
        builder.setMessage( "Are you sure you want to Logout" );
        builder.setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dbHelper.delete( DBHelper.dbLogin );
                intentFinish( context, MainActivity.class );
            }
        } );
        builder.setNegativeButton( "No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        } );
        builder.create().show();
    }

    //used in invward outward coureier.
    public void clickDate(final TextView _date) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get( Calendar.DAY_OF_MONTH );
        int month = cldr.get( Calendar.MONTH );
        int year = cldr.get( Calendar.YEAR );
        DatePickerDialog picker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear = monthOfYear + 1;
                String fm = "" + monthOfYear;
                String fd = "" + dayOfMonth;
                if (monthOfYear < 10) {
                    fm = "0" + monthOfYear;
                }
                if (dayOfMonth < 10) {
                    fd = "0" + dayOfMonth;
                }
//                        _Date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                String Date = fd + "-" + fm + "-" + year;
                _date.setText(Date);
            }
        }, year, month, day);

        picker.show();
        picker.getDatePicker().setMinDate( System.currentTimeMillis() );
    }
    public void clickDate() {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get( Calendar.DAY_OF_MONTH );
        int month = cldr.get( Calendar.MONTH );
        int year = cldr.get( Calendar.YEAR );
        DatePickerDialog picker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear = monthOfYear + 1;
                String fm = "" + monthOfYear;
                String fd = "" + dayOfMonth;
                if (monthOfYear < 10) {
                    fm = "0" + monthOfYear;
                }
                if (dayOfMonth < 10) {
                    fd = "0" + dayOfMonth;
                }
//                        _Date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                String Date = fd + "-" + fm + "-" + year;
                dateListener.getDate(Date);
            }
        }, year, month, day);
        picker.show();
    }
    public void monthPickerDilog(final EditText date){
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(context, new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                selectedMonth = selectedMonth + 1;
                String fm = "" + selectedMonth;
                if (selectedMonth < 10) {
                    fm = "0" + selectedMonth;
                }
                String Date =  fm + "-" + selectedYear;
                date.setText(Date);

            }
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));
        builder.setActivatedMonth(Calendar.MONTH)
                .setMinYear(1990)
                .setActivatedYear(2017)
                .setMaxYear(2030)
                .setMinMonth(Calendar.JANUARY)
                .setTitle("Select trading month")
                .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
                // .setMaxMonth(Calendar.OCTOBER)
                // .setYearRange(1890, 1890)
                // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
                //.showMonthOnly()
                // .showYearOnly()
                .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                    @Override
                    public void onMonthChanged(int selectedMonth) {
                        Log.d(TAG, "Selected month : " + selectedMonth);
                        // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                    @Override
                    public void onYearChanged(int selectedYear) {
                        Log.d(TAG, "Selected year : " + selectedYear);
                        // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                    }
                })
                .build()
                .show();
    }
}
