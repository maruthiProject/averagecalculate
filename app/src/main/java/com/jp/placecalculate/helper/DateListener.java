package com.jp.placecalculate.helper;

public interface DateListener {
    void getDate(String date);
}
