package com.jp.placecalculate.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.jp.placecalculate.R;
import com.jp.placecalculate.adapter.PlaceAdapter;
import com.jp.placecalculate.db.DBHelper;
import com.jp.placecalculate.model.StudentModel;

import java.util.List;

public class SearchActivity extends AppCompatActivity implements PlaceAdapter.ItemSelectedLisitner{
    AutoCompleteTextView search;
    DBHelper dbHelper;
    TextView result,register;
    private int searchCount;
    private String TAG=SearchActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        search=findViewById(R.id.search);
        result=findViewById(R.id.result);
        dbHelper=new DBHelper(this);
        register=findViewById(R.id.register);
        List<StudentModel> employeeModelList = dbHelper.getStudent();
        Log.e(TAG, "onCreate: size : "+employeeModelList.size() );
        PlaceAdapter employeeAdapter = new PlaceAdapter( this, R.layout.manager_item, R.id.name, employeeModelList
                , this );
        search.setThreshold( 1 );
        search.setAdapter(employeeAdapter);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


    @Override
    public void onItemClick(String id) {

    }

    @Override
    public void onClickCount(int count) {
        searchCount=count;
        if (count==0){
            result.setText("");
        }
    }

    @Override
    public void onClick(StudentModel items) {
        Log.e(TAG, "onClick: values : "+(items.getVal1()+items.getVal2()+items.getVal1()+
                items.getVal3()+items.getVal4()+items.getVal5()));
      int total_result= (items.getVal1()+items.getVal2()+
              items.getVal3()+items.getVal4()+items.getVal5())/5;
        Log.e(TAG, "onClick: total value : "+total_result );
        result.setText(String.valueOf(total_result));
    }
}
