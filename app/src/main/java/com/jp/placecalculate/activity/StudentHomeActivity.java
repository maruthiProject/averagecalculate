package com.jp.placecalculate.activity;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.jp.placecalculate.R;
import com.jp.placecalculate.db.DBHelper;
import com.jp.placecalculate.fragment.StudentRegFragment;
import com.jp.placecalculate.helper.CommonMethod;

public class StudentHomeActivity extends AppCompatActivity {

    CommonMethod commonMethod;
    String status="0";
    private TextView mTextMessage;
    DBHelper dbHelper;
    private String TAG=StudentHomeActivity.class.getSimpleName();
    NotificationManager notificationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home);
        commonMethod=new CommonMethod(this);
        dbHelper=new DBHelper(this);

        setFragment(new StudentRegFragment());
        Intent intent=getIntent();
        if (intent.getStringExtra("status")!=null){
            status=intent.getStringExtra("status");
        }
        Log.e(TAG, "onCreate: "+ status);
        /*if (status.equals("0")) {
            if (dbHelper.getSpcName(DBHelper.dbStudentDaily, DBHelper.attendance, DBHelper.userId, dbHelper.getSpcName(DBHelper.dbLogin, DBHelper.userId),
                    DBHelper.date, commonMethod.date()).equals("A")) {
                alertNotification(this);
            }
        }*/
    }

    private void setFragment(Fragment fragment) {

        // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        android.app.Fragment fragment = null;
        String title = getString(R.string.app_name);
        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            commonMethod.Logoutscreen();
            //    Toast.makeText(getApplicationContext(), " action is Cart!", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
       /* if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }*/
       finish();
    }

    public void alertNotification(Context context){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

       /* Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);*/
    }

}