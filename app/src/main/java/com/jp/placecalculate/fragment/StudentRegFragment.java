package com.jp.placecalculate.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jp.placecalculate.R;
import com.jp.placecalculate.db.DBHelper;
import com.jp.placecalculate.helper.CommonMethod;
import com.jp.placecalculate.helper.StaticData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import id.zelory.compressor.Compressor;

public class StudentRegFragment extends Fragment implements View.OnClickListener {
    View view;
    StaticData staticData;
    ArrayList<String> classes = new ArrayList<>();
    ArrayList<String> classId = new ArrayList<>();
    ArrayAdapter<String> classesAdapter;
    EditText staffId, placeName, value5, value4, studentId, department, yearPass, value1, value3, value2;
    CommonMethod commonMethod;
    TextView register;
    ImageView userImg;
    private String mCurrentPhotoPath;
    private int REQUEST_CAMERA = 786;
    private File actualImage;
    private File compressedImage;
    private Bitmap phtobitmap;
    DBHelper dbHelper;
    Spinner classesList;
    String randomNum;
    private String status, clgClasaId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_student_reg, container, false);
        staticData = new StaticData(getActivity());
        studentId = view.findViewById(R.id.studentId);
        staffId = view.findViewById(R.id.staffId);
        placeName = view.findViewById(R.id.placeName);
        value4 = view.findViewById(R.id.value4);
        value5 = view.findViewById(R.id.value5);
        userImg = view.findViewById(R.id.userImg);
        classesList = view.findViewById(R.id.classes);
        register = view.findViewById(R.id.register);
        value3 = view.findViewById(R.id.value3);
        department = view.findViewById(R.id.department);
        value2 = view.findViewById(R.id.value2);
        yearPass = view.findViewById(R.id.yearPass);
        value1 = view.findViewById(R.id.value1);
        commonMethod = new CommonMethod(getActivity());
        dbHelper = new DBHelper(getActivity());
        randomNum = commonMethod.randomNumber();
        studentId.setText("STUID" + randomNum);
        Bundle bundle = getArguments();
        if (bundle != null) {
            String status = bundle.getString("count");
            if (status.equals("1")) {
                status = "1";
            } else {
                status = "0";
            }
        }
        register.setOnClickListener(this);
        userImg.setOnClickListener(this);
        yearPass.setOnClickListener(this);
        return view;
    }

    private void validation() {
        String val4 = value4.getText().toString();
        String place = placeName.getText().toString();
        String val5 = value5.getText().toString();
        String val2 = value2.getText().toString();
        String val3 = value3.getText().toString();
        String val1 = value1.getText().toString();
        if (place.length() > 0) {
            placeName.setError(null);
            if (val1.length() > 0) {
                value1.setError(null);
                if (val2.length() > 0) {
                    value2.setError(null);
                    if (val3.length() > 0) {
                        value3.setError(null);
                        if (val4.length() > 0) {
                            value4.setError(null);
                            if (val5.length() > 0) {
                                value5.setError(null);

                                if (!dbHelper.checkData(DBHelper.dbStudent,DBHelper.placeName,place.toLowerCase().trim())) {
                                    dbHelper.insertstudent(commonMethod.randomNumber(), place.toLowerCase().trim(), val1, val2, val3, val4, val5);
                                    Toast.makeText(getActivity(), "Register success", Toast.LENGTH_SHORT).show();
                                    placeName.setText("");
                                    value1.setText("");
                                    value2.setText("");
                                    value3.setText("");
                                    value4.setText("");
                                    value5.setText("");
                                }else {
                                    Toast.makeText(getActivity(),"place Name already added pleace add a new place",Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                value5.requestFocus();
                                value5.setError("required value 5");
                            }
                        } else {
                            value4.requestFocus();
                            value4.setError("required value 4");
                        }
                    } else {
                        value3.requestFocus();
                        value3.setError("required value 3");
                    }
                } else {
                    value2.requestFocus();
                    value2.setError("required value 2");
                }
            } else {
                value1.requestFocus();
                value1.setError("required value 1");
            }
        } else {
            placeName.requestFocus();
            placeName.setError("required place Name");
        }
    }

    private void setFragment(Fragment _fragment) {
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
//        fragmentTransaction.replace(R.id.framlayout1, _fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register:
                validation();
                break;
            case R.id.userImg:
                captureImg();
                break;
            case R.id.yearPass:
                commonMethod.monthPickerDilog(yearPass);
                break;
        }
    }

    private void captureImg() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        actualImage = createImageFile();
        if (actualImage != null) {
            Uri photoURI;
            if (Build.VERSION.SDK_INT > 19) {
                photoURI = FileProvider.getUriForFile(getActivity(), "com.jp.aluminiregards.fileprovider", actualImage);
            } else {
                photoURI = Uri.fromFile(actualImage);
            }
            intent.putExtra("output", photoURI);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File createImageFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdir();
        }
        File image = new File(mediaStorageDir, "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg");
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            customphotoCompressImage();
        }
    }

    private void customphotoCompressImage() {
        if (actualImage != null) {
            try {
                compressedImage = new Compressor(getActivity()).setMaxWidth(540).setMaxHeight(500).setQuality(95).setCompressFormat(Bitmap.CompressFormat.JPEG).compressToFile(actualImage);
                setphtoCompressedImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setphtoCompressedImage() {
        FileNotFoundException e;
        phtobitmap = BitmapFactory.decodeFile(compressedImage.getAbsolutePath());
        try {
            FileOutputStream outputStream = new FileOutputStream(actualImage);
            FileOutputStream fileOutputStream;
            phtobitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            fileOutputStream = outputStream;
        } catch (FileNotFoundException e3) {
            e = e3;
            e.printStackTrace();

            commonMethod.imageView(userImg, phtobitmap, getActivity());
        }

        commonMethod.imageView(userImg, phtobitmap, getActivity());
    }

}
