package com.jp.placecalculate.model;

import java.io.Serializable;

public class StudentModel implements Serializable {

    String userId,placeName;
    int val1,val2,val3,val4,val5;

    public StudentModel(String userId, String placeName, int val1, int val2, int val3, int val4, int val5) {
        this.userId = userId;
        this.placeName = placeName;
        this.val1 = val1;
        this.val2 = val2;
        this.val3 = val3;
        this.val4 = val4;
        this.val5 = val5;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public int getVal1() {
        return val1;
    }

    public void setVal1(int val1) {
        this.val1 = val1;
    }

    public int getVal2() {
        return val2;
    }

    public void setVal2(int val2) {
        this.val2 = val2;
    }

    public int getVal3() {
        return val3;
    }

    public void setVal3(int val3) {
        this.val3 = val3;
    }

    public int getVal4() {
        return val4;
    }

    public void setVal4(int val4) {
        this.val4 = val4;
    }

    public int getVal5() {
        return val5;
    }

    public void setVal5(int val5) {
        this.val5 = val5;
    }
}
