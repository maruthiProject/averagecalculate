package com.jp.placecalculate.model;

import java.io.Serializable;

public class StaffModel implements Serializable {

    String title,message;
    byte[] image;

    public StaffModel(String title, String message, byte[] image) {
        this.title = title;
        this.message = message;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
