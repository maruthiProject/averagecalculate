package com.jp.placecalculate.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.jp.placecalculate.R;
import com.jp.placecalculate.model.StudentModel;

import java.util.ArrayList;
import java.util.List;

public class PlaceAdapter extends ArrayAdapter<StudentModel> {
    Context context;
    int resource, textViewResourceId;
    List<StudentModel> items, tempItems, suggestions;
    private ItemSelectedLisitner mListener;
    String className;
    String TAG=PlaceAdapter.class.getSimpleName();

    public interface ItemSelectedLisitner {
        void onItemClick(String id);
        void onClickCount(int count);
        void onClick(StudentModel items);
    }

    public PlaceAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<StudentModel> items,
                        ItemSelectedLisitner itemSelectedLisitner) {
        super(context, resource, textViewResourceId, items);
        this.mListener = itemSelectedLisitner;
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<StudentModel>(items); // this makes the difference.
        suggestions = new ArrayList<StudentModel>();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.manager_item, parent, false);
        }
        final StudentModel employeeModel = items.get(position);
        if (employeeModel != null) {
            TextView managerName = view.findViewById(R.id.name);
                managerName.setText(employeeModel.getPlaceName());

//            managerName.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mListener.onItemClick(employeeModel.getEmpId());
//                    Log.e(TAG, "onClick: empId : "+ employeeModel.getEmpId());
//                }
//            });

        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Log.e(TAG, "convertResultToString: "+resultValue );
            String str = null;
            /*if (className.equals(context.getResources().getString(R.string.visitor))) {
                str = ((StudentModel) resultValue).getPlaceName();
            }else if (className.equals(context.getResources().getString(R.string.gatepass))){
              str = ((StudentModel) resultValue).getPlaceName();
            }*/
            str = ((StudentModel) resultValue).getPlaceName();
            mListener.onClick((StudentModel) resultValue);
//            mListener.onItemClick(((DemoEmployeeModel) resultValue).getEmpId());
//            Log.e(TAG, "onClick: empId : "+ ((DemoEmployeeModel) resultValue).getEmpId());
            Log.e(TAG, "convertResultToString: "+str );
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                String check="";
                Log.e(TAG, "checkConvert: " );
                for (StudentModel employeeModel : tempItems) {
                    if (employeeModel.getPlaceName().toLowerCase().contains(constraint.toString().toLowerCase())
                            ||employeeModel.getPlaceName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(employeeModel);
                        check="1";
                        Log.e(TAG, "checkConvert: in" );
                    }else {
                        Log.e(TAG, "checkConvert: not" );
                        check="0";
                    }
                }
                FilterResults filterResults = new FilterResults();
//                if (check=="1") {
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
//                }
                /*else {
                    filterResults.values=;
                    filterResults.count = suggestions.size();
                }*/
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<StudentModel> filterList = (ArrayList<StudentModel>) results.values;
            mListener.onClickCount(results.count);
            if (results != null && results.count > 0) {
                clear();
                for (StudentModel employeeModel : filterList) {
                    add(employeeModel);
                    notifyDataSetChanged();
                    Log.e(TAG, "publishResults: "+employeeModel.getPlaceName() );
                    mListener.onItemClick(employeeModel.getPlaceName());
                }

            }else {
                Log.e(TAG, "publishResults null: "+results.count );
            }
        }
    };
}